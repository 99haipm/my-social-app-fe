import React from "react";

import { Form, Input, Button } from 'antd';
import { notification } from 'antd';
import './login-component.css'
import axios from "axios";
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

class LoginComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            basic_username:"",
            basic_password:"",
        }
    }

    openNotificationWithIcon = (type) => {
        var message  = {
            
        }
        if(type === 'success'){
            message = {
                message : "Đăng nhập thành công",
                description :"",
                placement:"topLeft",
            }
        }else{
            message = {
                message : "Đăng nhập thất bại",
                description :"",
                placement:"topLeft"
            }
        }
        return notification[type](message);
      };


    login = () =>{
        var data = {
            username : this.state.basic_username,
            password : this.state.basic_password
        }
        var header = {
            "content-type":"application/json"
        }
        axios.post('http://127.0.0.1:8000/api/users/login', data= JSON.stringify(data), {headers : header})
        .then(s =>{
            if(s.status === 200){
                localStorage.setItem('token','Bearer '+s.data.token);
                this.openNotificationWithIcon("success")
                window.location.href= "/"
            }
        }).catch(err =>{this.openNotificationWithIcon("error")})
    }

    onChange = (e) =>{
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    render() {
        return (
            <div className= "login-form">
                <h2 style={{textAlign:"center"}}>Login Form</h2>
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{ remember: true }}
                    onChange = {this.onChange}
                    onFinish = {this.login}
                >
                    <Form.Item
                        label="Username"
                        name="username"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                            Submit
                </Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }


}


export default LoginComponent