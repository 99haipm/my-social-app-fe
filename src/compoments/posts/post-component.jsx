
import { Avatar, Card, Carousel } from 'antd'

import React from "react";

import './post-component.css'
import { HeartTwoTone, MessageTwoTone } from '@ant-design/icons'
const { Meta } = Card
// const contentStyle = {
//     height: '160px',
//     color: '#fff',
//     lineHeight: '160px',
//     textAlign: 'center',
//     background: '#364d79',
// };
class PostComponent extends React.Component {


    render() {
        const imgs = this.props.imgs.map((elm,index) => {
            return (<div>
                        <img
                            className="post-image"
                            alt="example"
                            src={elm.imageUrl}
                        />
                    </div>)
        })
        return (
            <Card
                className="post"
                cover={
                    <Carousel className="carousel">
                       {imgs}
                    </Carousel>
                }
                actions={[
                    <MessageTwoTone key="comment" />,
                    <HeartTwoTone key="like" />,
                ]}
            >
                <Meta
                    avatar={<Avatar src={this.props.avatar} />}
                    title={this.props.username}
                    description={this.props.description}
                />
            </Card>
        )
    }



}


export default PostComponent