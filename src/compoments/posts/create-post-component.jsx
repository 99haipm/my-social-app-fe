import React from "react";
import { Button, Input } from 'antd';
import { Upload, Modal } from 'antd';
import { notification } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './post-component.css'
import myFirebase from '../../firebase'
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
const { TextArea } = Input;
class CreatePostComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileList: [

            ],
            description: "",
            urls: [
                
            ],
            loading: false
        }
    }

    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }


    onChange = (e) => {
        var uuid = uuidv4()
        var name = uuid + ".png"
        console.log(e)
        this.setState(prevState => ({
            fileList: [...prevState.fileList, {
                uid: uuid,
                name: name,
                status: 'done',
                file: e.file.originFileObj,
                originFileObj: e.file.originFileObj
            }]
        }))
    }


    beforeUpload = (file) => {
        const isImage = file.type.indexOf('image/') === 0;
        // You can remove this validation if you want
        const isLt5M = file.size / 1024 / 1024 < 5;
        return isImage && isLt5M;
    };

    customUpload = async ({ file }) => {
        // const storage = myFirebase.storage();
        // const metadata = {
        //     contentType: 'image/jpeg'
        // }
        // const storageRef = await storage.ref();

        // var uuid = uuidv4()
        // var name = uuid + ".png"
        // const imgFile = storageRef.child(`images/${uuidv4()}.png`);
        // try {
        //     const image = await imgFile.put(file, metadata);

        //     image.ref.getDownloadURL().then(s => {
        //         this.setState(prevState => ({
        //             fileList: [...prevState.fileList, {
        //                 uid: uuid,
        //                 name: name,
        //                 status: 'done',
        //                 url: s
        //             }]
        //         }))
        //         console.log(this.state.fileList)
        //     })
        // } catch (e) {
        //     console.log(e);
        // }
    };

    uploadImageToFirebase = async () => {
        const storage = myFirebase.storage();
        const metadata = {
            contentType: 'image/jpeg'
        }
        var storageRef = await storage.ref();
        try {
            for (let item in this.state.fileList) {
                var imgFile = storageRef.child(`images/${this.state.fileList[item].name}.png`);
                var image = await imgFile.put(this.state.fileList[item].file, metadata)
                let url = await image.ref.getDownloadURL();
                this.setState(prevState => ({
                    urls: [...prevState.urls, url]
                }))
            }
        } catch (e) {
            console.log(e);
            return false;
        }
        return true;
    }

    onTextChange = (e) => {
        this.setState({
            description: e.target.value
        })
    }

    onSubmitedForm = () => {
        this.setState({loading:true})
        this.uploadImageToFirebase().then(s => {
            if (s) {
                var data = {
                    description: this.state.description,
                    postImages: this.state.urls
                }
                var headers = {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoidXNlciIsInVzZXJJZCI6IjcwZDEwZDZhNGRhYTQ0ZmJhNzViMGMzNjk5NjgxYjJjIiwidXNlcm5hbWUiOiJtb29uZG8yIiwiZnVsbG5hbWUiOiJQaGFtIE1pbmggSGFpIiwiaXNzdWVyIjoiSW5zdGFncmFtQXBwIn0.Ch8Kh5S5xJvfVzxH2T0ClaHZNQt3UwpkkPOE3YcnOno",
                }
                axios.post("http://127.0.0.1:8000/api/posts", JSON.stringify(data), {
                    headers: headers
                }).then(s => {
                    if (s.status === 201) {
                        this.openNotificationWithIcon('success');
                        window.location.href = "/"
                    } else {
                        this.openNotificationWithIcon('error');
                    }
                })
            }
        })
    }

    openNotificationWithIcon = (type) => {
        var message  = {
            
        }
        console.log("in")
        if(type === 'success'){
            message = {
                message : "Tạo bài viết thành công",
                description :"",
                placement:"topLeft",
            }
        }else{
            message = {
                message : "Tạo bài thất bại",
                description :"",
                placement:"topLeft"
            }
        }
        return notification[type](message);
      };


    render() {
        const { previewVisible, previewImage, fileList, previewTitle } = this.state;
        const uploadButton = (
            <div>
                <PlusOutlined />
                <div style={{ marginTop: 8 }}>Upload</div>
            </div>
        );



        return (
            <div className="container-form-create">
                <h1>Tạo bài viết mới</h1>
                <h4>Nội dung:</h4>
                <TextArea rows={10} onChange={this.onTextChange} />
                <h4>Hình ảnh:</h4>
                <Upload
                    listType="picture-card"
                    fileList={this.state.fileList}
                    onPreview={this.handlePreview}
                    beforeUpload={this.beforeUpload}
                    onChange={this.onChange}
                >
                    {fileList.length >= 4 ? null : uploadButton}
                </Upload>
                <Modal
                    visible={previewVisible}
                    title={previewTitle}
                    footer={null}
                    onCancel={this.handleCancel}
                >
                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                </Modal>
                <Button type="primary" onClick={this.onSubmitedForm} loading={this.state.loading}>Tạo bài viết</Button>
            </div>

        )
    }



}


export default CreatePostComponent