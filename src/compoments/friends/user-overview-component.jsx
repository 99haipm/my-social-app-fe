

import { Avatar, Button, Col, Row } from 'antd';
import React from "react";
import './user-overview-component.css';

class UserOverviewComponent extends React.Component {

    render() {
        return (
            <div className="user-over-view">
                <Row>
                    <Col span={this.props.span[0]}>
                        <Avatar size={this.props.size} src={this.props.user.avatar} />
                    </Col>
                    <Col span={this.props.span[1]}>
                        <Row>
                            <h2 style={{fontSize:this.props.fontSize}}>{this.props.user.username}</h2>
                        </Row>
                        <Row>
                            <p style={{color:"#8e8e8e", fontSize:this.props.fontSize}}>{this.props.user.fullname}</p>
                        </Row>
                    </Col>
                    <Col span={this.props.span[2]}>
                        <Button type="link">
                            <b style={{fontSize:this.props.fontSize}}>{ this.props.useForRequest ? "Theo dõi" : "Chuyển"}</b>
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    }

}


export default UserOverviewComponent