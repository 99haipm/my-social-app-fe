import { Divider } from "antd";
import React from "react";
import UserOverviewComponent from "../friends/user-overview-component";
import './user-profile-right.css'
import axios from 'axios'

class UserProfileRightView extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            username:"",
            fullname:"",
            role:"",
            status:"",
            avatar:"",
            recommend:[]
        }
    }


    componentDidMount = () =>{
        this.fetchUser()
        this.getRecommendFriend()
    }


    fetchUser = () =>{
        var token = localStorage.getItem("token")
        if(token != null){
            var headers = {
                "Content-Type": "application/json",
                "Authorization": token.toString(),
            }
            axios.get("http://127.0.0.1:8000/api/users/fetch-user",{headers : headers})
            .then(s =>{
                console.log(s.data)
                if(s.status === 200){
                    return this.setState({
                        username:s.data.username,
                        fullname: s.data.fullname,
                        avatar: s.data.avatar,
                        status: s.data.status,
                        role:s.data.role
                    })
                }else{
                    console.log("error")
                }
            }).catch(err => console.log(err))
        }
    }

    getRecommendFriend = () =>{
        var token = localStorage.getItem("token")
        if(token != null){
            var headers = {
                "Content-Type": "application/json",
                "Authorization": token.toString(),
            }
            axios.get("http://127.0.0.1:8000/api/users/get-recommend",{headers : headers})
            .then(s =>{
                if(s.status === 200){
                    var data = s.data;
                    this.setState(prevState =>({
                      recommend: [...prevState.recommend, data]
                    }))
                }else{
                    console.log("error")
                }
            }).catch(err => console.log(err))
        }
    }

    
    render() {
        var data  = this.state.recommend.map((elm,index) =>{
            return <UserOverviewComponent size={30}
             span={[3, 8, 4]} 
             fontSize={12} 
             user={{ username: elm.data[index].username, 
            fullname: elm.data[index].fullname, 
            avatar: elm.data[index].avatar }} useForRequest={true} />
        })
        return (
            <div>
                <UserOverviewComponent size={55} span={[5, 8, 4]} fontSize={15} user={{ username: this.state.username, fullname: this.state.fullname, avatar: this.state.avatar }} useForRequest={false} />
                <div className="divider-text">
                    <Divider > <p style={{ color: "#8e8e8e" }}>Recommend for you</p></Divider>
                </div>
                {/* <UserOverviewComponent size={30} span={[3, 8, 4]} fontSize={12} user={{ username: "kimkhanh1410", fullname: "Phan Kim Khánh", avatar: "https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png" }} useForRequest={true} />
                <UserOverviewComponent size={30} span={[3, 8, 4]} fontSize={12} user={{ username: "doublet.tee", fullname: "Trần Gia Huy", avatar: "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png" }} useForRequest={true} />
                <UserOverviewComponent size={30} span={[3, 8, 4]} fontSize={12} user={{ username: "tnt.tzk", fullname: "Trần Nam Trung", avatar: "https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png" }} useForRequest={true} /> */}
                {data}
            </div>
        )
    }


}


export default UserProfileRightView