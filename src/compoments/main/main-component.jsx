

import { Alert, Col, Spin } from 'antd';
import PostComponent from '../posts/post-component'
import React from 'react'
import axios from 'axios';
import './main-component.css';
class MainComponent extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            currentPage: 0,
            isLoading: false,
            needLoad: false,
            isEnd: false
        }
    }

    componentDidMount = () => {
        window.removeEventListener('scroll', this.handleScroll);
        this.loadPosts(this.state.currentPage)
        window.addEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
        if (document.getElementsByClassName('ant-card ant-card-bordered post') !== null && document.getElementsByClassName('ant-card ant-card-bordered post') !== undefined) {
            var length = this.state.posts.length
            if (window.pageYOffset > (document.getElementsByClassName('ant-card ant-card-bordered post')[length - 1].offsetTop - 440)) {

                this.setState((prevState) => ({
                    isLoading: true,
                    currentPage: prevState.currentPage + 1,
                }))
                this.loadPosts(this.state.currentPage).then(v => {
                    if (v) {
                        window.addEventListener('scroll', this.handleScroll);
                    }else{
                        this.setState({
                            isEnd: true
                        })
                    }
                })
            } else {
                this.setState({
                    isLoading: false
                })
            }
        }
    }

    loadPosts = (page) => {
        var token = localStorage.getItem("token")
        if (token != null) {
            window.removeEventListener('scroll', this.handleScroll);
            var header = {
                "Content-type": "Application/json",
                "Authorization": token
            };
            var check = true;
            return axios.get(`http://127.0.0.1:8000/api/posts/get-posts?page=${page}`, { headers: header })
                .then(s => {
                    if (s.status === 200) {
                        if (s.data.data.length > 0) {
                            for (let index in s.data.data) {
                                this.setState(prevState => ({
                                    posts: [...prevState.posts, s.data.data[index]]
                                }))
                            }
                            check = true
                            return check
                        } else {
                            console.log("false")
                            check = false
                            return check
                        }
                    }
                })

        } else {
            return false;
        }
    }




    render() {
        const post = this.state.posts.map((elm, key) => {
            return <PostComponent username={elm.userId} avatar={elm.avatar} description={elm.description} imgs={elm.postImage} />
        })
        return (
            <Col span="13">
                <div className="content-left">
                    {/* <PostComponent username="hai pham" avatar="https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png" description="Welcome to my post" />
                    <PostComponent username="kim khanh" avatar="https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png" description="Welcome to my post" />
                    <PostComponent username="hai pham" avatar="https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png" description="Welcome to my post" />
                    <PostComponent username="kim khanh" avatar="https://cdn1.iconfinder.com/data/icons/avatar-97/32/avatar-02-512.png" description="Welcome to my post" /> */}
                    {post}
                    {
                        this.state.isEnd ? <Alert className="message-load"
                            message="Xem hết rồi"
                            type="success"
                        /> : <Spin size="large" className="spin-loading" spinning={this.state.isLoading} >

                        </Spin>
                    }

                </div>
            </Col>
        )
    }

}

export default MainComponent