// <!-- The core Firebase JS SDK is always required and must be listed first -->
// <script src="https://www.gstatic.com/firebasejs/8.6.5/firebase-app.js"></script>

// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->

// <script>
//   // Your web app's Firebase configuration
//   var firebaseConfig = {
//     apiKey: "AIzaSyAGAALDEv4gdY4AIRrlmL4wx-E54a7iTGA",
//     authDomain: "my-instagram-dab16.firebaseapp.com",
//     projectId: "my-instagram-dab16",
//     storageBucket: "my-instagram-dab16.appspot.com",
//     messagingSenderId: "39869978732",
//     appId: "1:39869978732:web:96b13710ba947abba6e049"
//   };
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
// </script>
import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyAGAALDEv4gdY4AIRrlmL4wx-E54a7iTGA",
  authDomain: "my-instagram-dab16.firebaseapp.com",
  projectId: "my-instagram-dab16",
  storageBucket: "my-instagram-dab16.appspot.com",
  messagingSenderId: "39869978732",
  appId: "1:39869978732:web:62a4c91f890d1041a6e049"
};
var myFirebase = firebase.initializeApp(firebaseConfig);
console.log(myFirebase)
export default myFirebase