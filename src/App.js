import './App.css';
import './compoments/nav-component.css'
import { Layout, Row, Col, Input, Button } from 'antd';
import { FileAddTwoTone, HeartTwoTone, MessageTwoTone, RightSquareTwoTone } from '@ant-design/icons'
import MainComponent from './compoments/main/main-component'
import CreatePostComponent from './compoments/posts/create-post-component'
import LoginComponent from './compoments/login-page/login-component'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import UserProfileRightView from './compoments/user-profile-right/user-profile-right';
const { Header, Content } = Layout;
const { Search } = Input


function App() {
  return (
    <Layout className="layout">
      <Header className="header">
        <Row type="flex">
          <Col span={8} style={{ height: "10vh" }}>
            <div className="logo">
            </div>
          </Col>
          <Col span={8}>
            <div className="search">
              <Search placeholder="search" style={{ width: 200 }} color="white" />
            </div>
          </Col>
          <Col span={8}>
            <Row>
              <div style={{ fontSize: "30px", marginTop: "1vh", marginRight: "10px", alignItems: "flex-end", display: "flex" }}>
                <Button type="primary" shape="circle" icon={<HeartTwoTone />} />
              </div>
              <div style={{ fontSize: "30px", marginTop: "1vh", marginRight: "10px", alignItems: "flex-end", display: "flex" }}>
                <Button type="primary" shape="circle" icon={<MessageTwoTone />} />
              </div>
              <div style={{ fontSize: "30px", marginTop: "1vh", marginRight: "10px", alignItems: "flex-end", display: "flex" }}>
                <Button type="primary" href="/create-post" shape="circle" icon={<FileAddTwoTone />} />
              </div>
              <div style={{ fontSize: "30px", marginTop: "1vh", marginRight: "10px", alignItems: "flex-end", display: "flex" }}>
                <Button type="primary" onClick={() =>{localStorage.removeItem('token'); window.location.href="/login"}} shape="circle" icon={<RightSquareTwoTone />} />
              </div>
            </Row>
          </Col>
        </Row>
      </Header>
      <Content className="content" style={{ height: "auto" }}>
        <div className="content">
          <Row>
            <BrowserRouter>
              <Switch>
                <Route exact={true} path="/">
                  <MainComponent />
                  <Col span="11">
                    <div className="content-right">
                      <UserProfileRightView />
                    </div>
                  </Col>
                </Route>
                <Route exact={true} path="/login">
                  <Col span="13">
                    <div className="content-left">
                      <LoginComponent />
                    </div>
                  </Col>

                </Route>
                <Route exact={true} path="/create-post">
                  <Col span="13">
                    <div className="content-left">
                      <CreatePostComponent />
                    </div>

                  </Col>
                  <Col span="11">
                    <div className="content-right">
                      <UserProfileRightView />
                    </div>
                  </Col>
                </Route>
              </Switch>
            </BrowserRouter>

          </Row>
        </div>
      </Content>
    </Layout>
  );
}

export default App;
